#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

struct image rotate( struct image img );

#endif //IMAGE_TRANSFORMER_ROTATE_H
