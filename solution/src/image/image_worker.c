#include "image/image_worker.h"

struct image create_image(uint32_t width, uint32_t height){

    size_t pixel_size = sizeof(struct pixel);

    struct pixel* data = malloc(width*height * pixel_size);

    if (data){
        return (struct image) {width, height, data};
    }
    return (struct image) {0};
}

size_t get_padding_from_img(struct image const *img) {
    return (PIXEL_SIZE - ((sizeof(struct pixel) * img->width) % PIXEL_SIZE));
}


void free_img(struct image *img) {
    free(img->data);
    free(img);

}


