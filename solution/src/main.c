#include "bmp/bmp_io.h"
#include "bmp/io_statuses.h"
#include "image/image_worker.h"
#include "image/rotate.h"

#include "stdio.h"
#include "stdlib.h"

void close_file(FILE *file) {
    fclose(file);
}


int main(int argc, char **argv) {

    if (argc != 3) {
        fprintf(stderr, "3 params are expected");
        return 1;
    }

    FILE *src_file;
    struct image *src_img;

    // Открываем файл для чтения

    const char *src_filename = argv[1];

    src_file = fopen(src_filename, "rb");

    if (!src_file) {
        fprintf(stderr, "Couldn't open source file");
        return 1;
    }

    src_img = malloc(sizeof(struct image));

    // Считываем source image
    enum read_status read_status = from_bmp(src_file, src_img);


    if (read_status == READ_OK) {
        fprintf(stdout, "Прочитал");

        // Получаем новую перевернутую image

        struct image new_img = rotate(*src_img);

        // Открываем файл для сохранения новой картинки
        FILE *out_file;
        const char *out_filename = argv[2];

        out_file = fopen(out_filename, "wb");

        if (out_file == NULL) {
            fprintf(stderr, "Couldn't open out file");

            close_file(src_file);
            free_img(src_img);
            free_img(&new_img);

            return 1;
        }


        to_bmp(out_file, &new_img);

        fprintf(stdout, "Success!");

        close_file(out_file);
        close_file(src_file);
        free_img(src_img);
        free(new_img.data);

        return 0;
    }

    fprintf(stderr, "Couldn't read source file");

    close_file(src_file);
    free_img(src_img);


    return 1;
}


