#include "bmp/bmp_io.h"
#include "bmp/io_statuses.h"
#include "image/image_worker.h"


#include "stdio.h"
#include "stdlib.h"

struct __attribute__((packed)) bmp_header{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

static struct bmp_header create_bmp_header(struct image const *img) {
    size_t bmp_header_size = sizeof(struct bmp_header);
    size_t padding_size = get_padding_from_img(img);
    size_t pixel_size = sizeof(struct pixel);
    return (struct bmp_header) {
            .bfType = 19778,
            .bfReserved = 0,
            .biCompression = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biPlanes = 0,
            .biXPelsPerMeter = 2834,
            .biYPelsPerMeter = 2834,
            .biClrImportant = 0,
            .biClrUsed = 0,
            .biBitCount = 24,
            .biHeight = img->height,
            .biWidth = img->width,
            .biSizeImage = pixel_size * img->height * (img->width + padding_size),
            .bfileSize = bmp_header_size * pixel_size * img->height * (img->width + padding_size)
    };
}


enum read_status from_bmp(FILE *in, struct image *img) {

    if (!in || !img) {
        return READ_INVALID_ARGUMENTS;
    }

    size_t bmp_header_size = sizeof(struct bmp_header);

    struct bmp_header *bmp_header = malloc(bmp_header_size);

    if (fread(bmp_header, bmp_header_size, 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    *img = create_image(bmp_header->biWidth, bmp_header->biHeight);


    free(bmp_header);

    for (uint32_t i = 0; i < img->height; i++) {
        struct pixel *read_pos = img->data + i * img->width;
        size_t read_status = fread(read_pos, sizeof(struct pixel), img->width, in);
        if (read_status != img->width) {
            return READ_INVALID_BITS;
        }
        fseek(in, (int8_t) get_padding_from_img(img), SEEK_CUR);
    }

    return READ_OK;

}

enum write_status to_bmp(FILE *out, struct image const *img) {
    if (!out || !img) {
        return WRITE_ERROR;
    }

    size_t bmp_header_size = sizeof(struct bmp_header);
    size_t padding_size = get_padding_from_img(img);
    size_t pixel_size = sizeof(struct pixel);


    struct bmp_header bmpHeader = create_bmp_header(img);

    if (fwrite(&bmpHeader, bmp_header_size, 1, out) != 1) {
        return WRITE_ERROR;
    }

    char empty_arr[4] = {0};
    for (uint32_t y = 0; y < img->height; y++) {
        if (fwrite(img->data + 0 + y * img->width, pixel_size, img->width, out) != img->width) {
            return WRITE_ERROR;
        }
        if (fwrite(empty_arr, sizeof(char), padding_size, out) != padding_size) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;

}





