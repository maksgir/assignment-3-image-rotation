#include "image/rotate.h"
#include "image/image_worker.h"

#include "stdlib.h"

struct image rotate(struct image img) {

    struct image new_img = create_image(img.height, img.width);


    if (new_img.data) {
        for (uint64_t i = 0; i < new_img.width; i++) {
            for (uint64_t j = 0; j < new_img.height; j++) {
                new_img.data[new_img.width + j * img.height - i - 1] = img.data[j + img.width * i];
            }
        }

    }
    return new_img;
}
