

#ifndef IMAGE_TRANSFORMER_IO_STATUSES_H
#define IMAGE_TRANSFORMER_IO_STATUSES_H

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_ARGUMENTS
    /* коды других ошибок  */
};


#endif //IMAGE_TRANSFORMER_IO_STATUSES_H
