#include "image/image_worker.h"
#include "io_statuses.h"



#include "stdio.h"
#include "stdlib.h"

#ifndef IMAGE_TRANSFORMER_BMP_IO_H
#define IMAGE_TRANSFORMER_BMP_IO_H



enum read_status from_bmp( FILE* in, struct image* img );

enum write_status to_bmp( FILE* out, struct image const* img );

#endif //IMAGE_TRANSFORMER_BMP_IO_H
