#include "stdint.h"
#include "stdio.h"

#include "stdlib.h"

#ifndef IMAGE_TRANSFORMER_CREATE_IMAGE_H
#define IMAGE_TRANSFORMER_CREATE_IMAGE_H

#define PIXEL_SIZE 4

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image create_image(uint32_t width, uint32_t height);

size_t get_padding_from_img(struct image const *img);

void free_img(struct image *img) ;




#endif //IMAGE_TRANSFORMER_CREATE_IMAGE_H
